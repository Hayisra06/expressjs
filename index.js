const express = require("express"); //import express
const res = require("express/lib/response");
const app = express(); //create an instance of express

//Get PORT from environtment (default   8000)
const PORT = process.env.PORT || 8000;

const BOOK_MSG = `Thanks to adding book into database`;

//add a middleware
function isAdmin(req, res, next){
    if(req.query.iam === 'admin'){
        next();
        return;
    }
}
//to active req.body
app.use(express.urlencoded());

app.get("/", (req, res) => {
    res.render("index.ejs",{
        name : req.query.name || "Guest",
    });
});

// if client accsess (Get)http://localhost:8000/. it will go to this function
app.get("/", (req, res) => {
    console.log("Muncul setiap kali dijalankan");
    res.send("Hello Ais");
});

 // if client accsess (Get)http://localhost:8000/:id
app.get("/api/v1/books/:id", isAdmin, (req, res) => {
    console.log(req.params);
    res.send(`Your request the book with  : ${req.params.id}`);
});

// if client accsess (Get)http://localhost:8000/:id
app.get("/api/v1/books/", isAdmin,(req, res) => {
    console.log(req.query);
    res
    .status(200)
    .send(`Your request the book with  : ${req.query.name}`);
});

// if client accsess (Post)http://localhost:8000/:id
app.post("/api/v1/books/", isAdmin,(req, res) => {
    console.log(req.body);
    res.status(201).send(BOOK_MSG);
});

//this syntax to run the server whit port 8888
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});